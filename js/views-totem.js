jQuery(function($) {
  $('.views_totem ul.item-list').totemticker({
    row_height: '25px',
    next: '#ticker-next',
    previous: '#ticker-previous',
    mousestop: true
  });
});