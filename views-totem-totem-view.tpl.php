<div class="views_totem views_totem-<?php print $view->vid; ?> clearfix">
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <a id="ticker-next"><?php print t('Next'); ?></a>
  <a id="ticker-previous"><?php print t('Previous'); ?></a>
  <ul class="item-list clearfix">
    <?php foreach ($rows as $id => $row): ?>
      <li class="<?php print 'item-' . $id; ?>"><?php print $row; ?></li>
    <?php endforeach; ?>
  </ul>
</div>
