<?php

class views_plugin_style_totem extends views_plugin_style {

  //default options
  function option_definition() {
    $options = parent::option_definition();
    $options['next'] = array('default' => '#nextbutton');
    $options['previous'] = array('default' => '#prevbutton');
    $options['stop'] = array('default' => '');
    $options['start'] = array('default' => '');
    $options['row_height'] = array('default' => '100px');
    $options['speed'] = array('default' => 800);
    $options['interval'] = array('default' => 40000);
    $options['max_items'] = array('default' => '');
    $options['mousestop'] = array('default' => FALSE);
    $options['direction'] = array('default' => 'down');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['next'] = array(
        '#type' => 'textfield',
        '#title' => t('Next'),
        '#description' => t('CSS ID\'s of next and previous buttons. Next scrolls the ticker down, and previous scrolls up.'),
        '#default_value' => $this->options['next'],
    );
    $form['previous'] = array(
        '#type' => 'textfield',
        '#title' => t('Previous'),
        '#description' => t('CSS ID\'s of next and previous buttons. Next scrolls the ticker down, and previous scrolls up.'),
        '#default_value' => $this->options['previous'],
    );
    $form['stop'] = array(
        '#type' => 'textfield',
        '#title' => t('Stop'),
        '#description' => t('CSS ID\'s of stop and start buttons.'),
        '#default_value' => $this->options['stop'],
    );
    $form['start'] = array(
        '#type' => 'textfield',
        '#title' => t('Start'),
        '#description' => t('CSS ID\'s of stop and start buttons.'),
        '#default_value' => $this->options['start'],
    );
    $form['row_height'] = array(
        '#type' => 'textfield',
        '#title' => t('Row height'),
        '#description' => t('Height of each ticker row in PX. Should be uniform and formatted as a string.'),
        '#default_value' => $this->options['row_height'],
    );
    $form['speed'] = array(
        '#type' => 'textfield',
        '#title' => t('Speed'),
        '#description' => t('Speed of transition animation in milliseconds.'),
        '#default_value' => $this->options['speed'],
    );
    $form['interval'] = array(
        '#type' => 'textfield',
        '#title' => t('interval'),
        '#default_value' => $this->options['interval'],
    );
    $form['max_items'] = array(
        '#type' => 'textfield',
        '#title' => t('Max items'),
        '#description' => t('Optional. Integer for how many items to display at once. Resizes height and overflow clipping accordingly. '),
        '#default_value' => $this->options['max_items'],
    );
    $form['mousestop'] = array(
        '#type' => 'checkbox',
        '#title' => t('mousestop'),
        '#description' => t('If set to true, the ticker will stop while mouse is hovered over it.'),
        '#default_value' => $this->options['mousestop'],
    );
    $form['direction'] = array(
        '#type' => 'select',
        '#title' => t('Direction'),
        '#options' => array(
            'down' => t('Down'),
            'up' => t('Up'),
        ),
        '#required' => TRUE,
        '#description' => t('Direction that list will scroll (down or up)'),
        '#default_value' => $this->options['direction'],
    );
  }

  function options_validate(&$form, &$form_state) {
    parent::options_validate($form, $form_state);
    $options = &$form_state['values']['style_options'];
  }

}
