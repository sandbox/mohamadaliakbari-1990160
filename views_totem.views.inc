<?php

/* hook_views_plugins */

function views_totem_views_plugins() {
  return array(
      'module' => 'views_totem',
      'style' => array(
          'totem' => array(
              'title' => t('Totem'),
              'handler' => 'views_plugin_style_totem',
              'uses options' => TRUE,
              'help' => t('Render using totem plugin.'),
              'theme' => 'views_totem_totem_view',
              'theme path' => drupal_get_path('module', 'views_totem'),
              'type' => 'normal',
              'uses row plugin' => TRUE,
          ),
      ),
  );
}